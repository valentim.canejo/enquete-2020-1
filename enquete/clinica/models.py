from django.db import models


class Ficha(models.Model):
	nome = models.CharField(max_length=80)
	raca = models.CharField(max_length=80)
	idade = models.IntegerField()
	contato_dono = models.CharField(max_length=80)
	situacao = models.TextField()
	data_cadastro = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return self.nome

class Registro(models.Model):
	titulo = models.CharField(max_length=80,default='titulo')
	diagnostico = models.TextField()

	data_registro = models.DateTimeField(auto_now_add=True)


	ficha = models.ForeignKey(Ficha, on_delete = models.CASCADE)

	def __str__(self):
		return self.titulo
