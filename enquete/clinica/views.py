from django.shortcuts import render, get_object_or_404, redirect
from django.core.paginator import Paginator
from django.contrib.auth.decorators import login_required
from .models import Registro, Ficha
from .forms import FichaForm

def index(request):
    return render(request, 'clinica/index.html', {})


def criarFicha(request):

	if request.method == 'POST':
		form = FichaForm(request.POST)

		if form.is_valid():
			ficha = form.save(commit=False)

			ficha.save()
			return redirect('/clinica/listarFichas')

	ficha = FichaForm()
	return render(request, 'clinica/criarFicha.html', {'ficha': ficha})




def listarFichas(request):

	fichas = Ficha.objects.all().order_by('-data_cadastro')

	return render(request, 'clinica/listarFichas.html', {'fichas': fichas})


@login_required
def ficha(request, id):

	ficha = get_object_or_404(Ficha, pk=id)

	search = request.GET.get('search')

	if search:

		registros = Registro.objects.filter(titulo__icontains=search)

	else:


		registros = Registro.objects.filter(ficha= id).order_by('-data_registro')

		paginator = Paginator(registros, 3)

		page = request.GET.get('page')

		registros = paginator.get_page(page)

	return render(request, 'clinica/ficha.html', {'registros': registros, 'ficha':ficha, 'registro':registro})


@login_required
def registro(request, id):
	registro = get_object_or_404(Registro, pk=id)
	return render(request, 'clinica/registro.html', {'registro': registro})



@login_required
def novoregistro(request, id):
	ficha = get_object_or_404(Ficha, pk=id)
	titulo = request.POST['titulo']
	diagnostico = request.POST['diagnostico']


	registro = Registro.objects.create(ficha=ficha, titulo=titulo, diagnostico=diagnostico)
	registro.save()
	return redirect('clinica:ficha', id)


@login_required
def cadastrarRegistro(request, id):
	ficha = get_object_or_404(Ficha, pk=id)
	return render(request, 'clinica/novoregistro.html', {'ficha':ficha})