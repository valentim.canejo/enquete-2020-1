from django.urls import path, include
from . import views

app_name = 'clinica'
urlpatterns = [
    path('', views.index, name = 'index'),
    path('ficha/<int:id>', views.ficha, name="ficha"),
    path('listarFichas/', views.listarFichas, name="listarFichas"),
    path('criarFicha/', views.criarFicha, name="criarFicha"),
    path('registro/<int:id>', views.registro, name="registro"),
    path('novoregistro/<int:id>', views.novoregistro, name="novoregistro"),
    path('cadastrarRegistro/<int:id>', views.cadastrarRegistro, name="cadastrarRegistro"),
    path('accounts/', include('django.contrib.auth.urls')),

]
