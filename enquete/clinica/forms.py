from django import forms

from .models import Registro, Ficha

class RegistroForm(forms.ModelForm):

	class Meta:
		model = Registro
		fields = ('titulo', 'diagnostico')

class FichaForm(forms.ModelForm):

	class Meta:
		model = Ficha
		fields = ['nome', 'raca', 'idade', 'contato_dono', 'situacao']
