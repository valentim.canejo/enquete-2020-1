from django.contrib import admin
from .models import Registro, Ficha

admin.site.register(Registro)
admin.site.register(Ficha)
