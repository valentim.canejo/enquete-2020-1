import datetime
from django.utils import timezone
from django.test import TestCase
from django.urls import reverse
from .models import Pergunta

class PerguntaTeste(TestCase):
    def test_publicada_recentemente_com_pergunta_no_futuro(self):

        data_futura = timezone.now() + datetime.timedelta(seconds=1)
        pergunta_futura = Pergunta(data_publicacao = data_futura)
        self.assertIs(pergunta_futura.publicada_recentemente(), False)

    def test_publicado_recentemente_com_data_anterior_a_24h_no_passado(self):

        data_passada = timezone.now() - datetime.timedelta(days=1, seconds=1)
        pergunta_passado = Pergunta(data_publicacao = data_passada)
        self.assertIs(pergunta_passado.publicada_recentemente(), False)

    def test_publicada_recentemente_com_data_nas_ultimas_24h(self):

        data = timezone.now() - datetime.timedelta(hours=23, minutes=59, seconds=59)
        pergunta_ok = Pergunta(data_publicacao = data)
        self.assertIs(pergunta_ok.publicada_recentemente(), True)

def criar_pergunta(texto, dias):

        data = timezone.now() + datetime.timedelta(days=dias)
        return Pergunta.objects.create(texto=texto, data_publicacao=data)

class IndexViewTeste(TestCase):

    def test_sem_perguntas_cadastradas(self):

        resposta = self.client.get(reverse('main:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, "Não há enquetes cadastradas até o momento!")
        self.assertQuerysetEqual(resposta.context['pergunta_list'], [])

    def test_com_pergunta_no_passado(self):

        criar_pergunta(texto='Pergunta no passado', dias=-30)
        resposta = self.client.get(reverse('main:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertQuerysetEqual(
            resposta.context['pergunta_list'], ['<Pergunta: Pergunta no passado>']
        )

    def test_com_pergunta_no_futuro(self):

        criar_pergunta(texto="Pergunta no futuro", dias=1)
        resposta = self.client.get(reverse('main:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, "Não há enquetes cadastradas até o momento!")
        self.assertQuerysetEqual(resposta.context['pergunta_list'], [])

    def test_pergunta_no_passado_e_no_futuro(self):

        criar_pergunta(texto="Pergunta no passado", dias=-1)
        criar_pergunta(texto="Pergunta no futuro", dias=1)
        resposta = self.client.get(reverse('main:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, "Pergunta no passado")
        self.assertQuerysetEqual(resposta.context['pergunta_list'],
            ['<Pergunta: Pergunta no passado>']
        )

    def test_duas_perguntas_no_passado(self):

        criar_pergunta(texto="Pergunta no passado 1", dias=-1)
        criar_pergunta(texto="Pergunta no passado 2", dias=-5)
        resposta = self.client.get(reverse('main:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, "Pergunta no passado")
        self.assertQuerysetEqual(resposta.context['pergunta_list'],
            ['<Pergunta: Pergunta no passado 1>',
            '<Pergunta: Pergunta no passado 2>']
        )

class DetalhesViewTeste(TestCase):

    def test_pergunta_no_futuro(self):

        pergunta_futura = criar_pergunta(texto="Pergunta no futuro", dias=5)
        resposta = self.client.get(
            reverse('main:detalhes', args=[pergunta_futura.id,])
        )
        self.assertEqual(resposta.status_code, 404)

    def test_pergunta_no_passado(self):

        pergunta_passada = criar_pergunta(texto="Pergunta no passado", dias=-1)
        resposta = self.client.get(
            reverse('main:detalhes', args=[pergunta_passada.id,])
        )
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, pergunta_passada.texto)





