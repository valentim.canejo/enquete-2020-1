# Generated by Django 2.2.7 on 2020-11-06 22:46

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Comentario',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome_autor', models.CharField(max_length=200)),
                ('texto', models.CharField(max_length=200)),
                ('data_insercao', models.DateTimeField(verbose_name='Data de Publicação')),
                ('Pergunta', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.Pergunta')),
            ],
        ),
    ]
